package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    double toFahrenheit(String cel) {
        double dcel = Double.parseDouble(cel);
        return (dcel * 9 / 5 + 32);
    }
}