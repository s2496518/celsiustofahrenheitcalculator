package nl.utwente.di.bookQuote;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
/***Tests the Quoter***/
public class TestQuoter {
        @Test
        public void testTemp1()throws Exception {
        Quoter quoter = new Quoter();
        double C = quoter.toFahrenheit("1") ;
        Assertions.assertEquals (33.8, C , 0.0 , "1C_to_F" ) ;
        }
}